import socket
from threading import Event


class EchoHandler(object):
    default_port = 33333
    package_size = 1024
    stop_command = 'disconnect\r\n'

    def __init__(self, port=default_port):
        self._port = port

    def __call__(self, conn, stop_event):
        while not stop_event.is_set():
            data = conn.recv(self.package_size)

            if(data == self.stop_command or not data):
                break
            conn.sendall(data)
        conn.close()

    @property
    def port(self):
        return self._port
