import socket, os, re
from threading import Event
from datetime import datetime


class HttpHandler(object):
    default_port = 18080
    package_size = 1024
    default_host = '127.0.0.1'
    defoult_dir = 'var/www/localhost'
    statuses = {
         '405': 'Method Not Allowed',
         '404': 'Not Found',
         '200': 'OK',
        }
    ok_template =\
"""
Connection: close
Content-Length: {}
Content-Type: {}
Date: {}\r\n\r\n"""

    def __init__(self, port=default_port):
        config = dict()
        self._port = port
        config_file = open('http.conf', 'r')

        for line in config_file:
            if not line.strip() == '':
                (key, value) = tuple(line.split())
                config[key] = value

        if "host" in config.keys():
            self.host = config['host']
        else:
            self.host = self.default_host
        if "port" in config.keys():
            self._port = int(config['port'])
        else:
            self.port = self.default_port
        if "root_dir" in config.keys():
            self.root_dir = re.sub("^/|/$", "", config['root_dir'])
        else:
            self.root_dir = self.defoult_dir
        self.root_dir = os.path.join(os.getcwd(), self.root_dir)
        config_file.close()

        self.mime_types = dict()
        mime_types_file = open('mime.types', 'r')
        for line in mime_types_file:
            if not line.strip() == '':
                (key, value) = tuple(line.split(None, 1))
                self.mime_types[key] = value.strip()
        mime_types_file.close()

    def get_file(self, requested_file):
        if requested_file == '/':
            requested_file = 'index.html'
        requested_file = re.sub("^/", "", requested_file)
        file_path = os.path.join(self.root_dir, requested_file)

        founded_file = open(file_path, 'rb')

        return (founded_file)

    def __call__(self, conn, stop_event):
        data = conn.recv(self.package_size)
        request_line = data.split('\r\n', 1)[0]
        (method, requested_file, http_version) = tuple(request_line.split())

        try:
            founded_file = self.get_file(requested_file)
            file_type = founded_file.name.rsplit('.', 1)[1]
            if file_type not in self.mime_types.keys() or method != 'GET':
                status_code = '405'
            else:
                status_code = '200'
        except IOError:
            status_code = '404'
        response = ' '.join((status_code, self.statuses[status_code],
                             http_version))
        if status_code == '200':
            founded_file.seek(0, os.SEEK_END)
            file_size = founded_file.tell()
            founded_file.seek(0, os.SEEK_SET)
            response += self.ok_template.format(
                file_size,
                self.mime_types[file_type],
                datetime.utcnow().strftime("%a, %d %b %Y %H:%M:%S GMT")
                )

            conn.sendall(response)
            package = founded_file.read(self.package_size)
            while package and not stop_event.is_set():
                conn.send(package)
                package = founded_file.read(self.package_size)
        else:
            conn.sendall(response)

        print ' '.join((status_code, requested_file))
        conn.close()

    @property
    def port(self):
        return self._port
