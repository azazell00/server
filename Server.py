import socket
import signal
from threading import Event, Thread


class Server(object):
    max_users = 5

    def __init__(self, handler):
        self.stop_event = Event()
        self.handler = handler
        self.soc = socket.socket()
        self.listening_thread = None

    def accept_clients(self, stop_event):
        while not stop_event.is_set():
            conn = self.soc.accept()[0]
            thread = Thread(target=self.handler, args=(conn, self.stop_event))
            thread.start()

    def start(self):
        self.stop_event.clear()
        self.listening_thread = Thread(target=self.accept_clients,
                                       args=(self.stop_event,))

        self.soc.bind(('', self.handler.port))
        self.soc.listen(self.max_users)
        self.listening_thread.start()
        signal.signal(signal.SIGINT, self.stop)
        print("Press Ctrl+C to quit")
        signal.pause()

    def stop(self, signal, frame):
        if self.is_running:
            self.stop_event.set()
        self.listening_thread = None

    @property
    def is_running(self):
        if self.listening_thread:
            return True
        else:
            return False
